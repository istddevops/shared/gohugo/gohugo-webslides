# Hugo-Webslides

Working copy of [Hugo-Weblides Theme](https://github.com/RCJacH/hugo-webslides) as a Go Module.

Current `copy is from` [Commit **86ef389**](https://github.com/RCJacH/hugo-webslides/commit/86ef38925f97e866ea491314d690bc4c57302b6a) on `23 feb 2022`.
